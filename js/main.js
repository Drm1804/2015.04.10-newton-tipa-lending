/*Слайдер на 1-м скроле*/

function mainSlider(){
    var scrollBg = document.querySelector('.b-scroll-1'); //Блок, в котором будем менять Бэкграунд
    var textBox = document.getElementsByClassName('b-sc1-box__text'); //Блоки с тектовой текстовой информацией
    var navBox = document.querySelector('.b-sc1-box-nav'); //Родительский блок, элементов переключения
    var navDots = document.getElementsByClassName('b-sc1-box-nav__li'); //Элементы переключения

    //Функция переключения


    function changeSlide(number){

        //Меням бэкграунд
        for (i=0; i < navDots.length; i++){
            scrollBg.classList.remove('b-scroll-1--box' + i);
        }

        scrollBg.classList.add('b-scroll-1--box' + number);

        //Меняем текстовый блок
        for (i=0; i < textBox.length; i++){
            if (i == number){
                textBox[number].style.display = 'block';
                textBox[number].style.opacity = 1;
            } else {
                textBox[i].style.display = 'none';
                textBox[i].style.opacity = 0;
            }
        }
        //Перекрашиваем элементы управления
        for(i=0; i < navDots.length; i++){
            navDots[i].classList.remove('b-sc1-box-nav__li--active');
        }
        navDots[number].classList.add('b-sc1-box-nav__li--active');

    }

    //Ивент переключения
    navBox.addEventListener('click', function(e){
        var number = 0;
        var node = e.target;
        if(node != navBox){
            //Если клик произошел по пунктам навигации, то определяем номер пункта навигации и передаем это значение в переменную number
            for(i=0; i < navDots.length; i++){
                if(node == navDots[i]){
                    break;
                } else {
                    number++;
                }
            }
        } else {
            //Если клик произошел по родительскому блоку, то ничего не делаем
            return;
        }
        changeSlide(number);
    }, false);
}

window.addEventListener('load', function(){
        mainSlider()
    }, false);

/*

События при скроле

 */


//Появление fixedHeader
function showHeader(){
    var headerBlock = document.querySelector("header");
    var mainSectionToTop = document.querySelector("#section1").getBoundingClientRect().top ,
        headerNav =document.querySelector('.header-navig');

    if(mainSectionToTop <= 0){
        headerNav.classList.add('header--fixed');
        setTimeout(function(){
            headerNav.style.top = 0
        } , 30);
        //headerBlock.style.top = 0;
    } else {
        headerNav.classList.remove('header--fixed');
        headerNav.style.top = '-140px';
    }

}



//Появление ToTop
function showToTop(){
    var documentHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );
    var userScroll = window.pageYOffset;
    var butScrollToTop = document.querySelector('.scrollToTop');

    if (userScroll > 0.05*documentHeight){
        butScrollToTop.style.display = 'block';
        //И запускаем функцию поднятия наверх
        scrollToTop();
    } else {
        butScrollToTop.style.display = 'none';
    }
}
//Перемотка На верх
function scrollToTop(){
    var butScrollToTop = document.querySelector('.scrollToTop');
    butScrollToTop.addEventListener('click', function(){
        window.scrollTo(0,0);
    } , false)

}

//Выдедение пунктов навигации
function navChecked(){
    var allToScrollObject = document.querySelectorAll(".b-wrapper__section");
    for (i=0; i < allToScrollObject.length; i++){
        if (allToScrollObject[i].getBoundingClientRect().top - 140 <= 0 && allToScrollObject[i].getBoundingClientRect().bottom - 140 >=1){
            document.querySelector('a[href^="#section'+i+'"]').classList.add('b-head-nav-item--check');
        } else {
            if ( document.querySelector('a[href^="#section'+i+'"]').classList.contains('b-head-nav-item--check')){
                document.querySelector('a[href^="#section'+i+'"]').classList.remove('b-head-nav-item--check');
            }
        }
    }
}

document.addEventListener('scroll', function(e){
    showHeader();
    showToTop();
    navChecked();

}, false);

/*
Плавный скролинг по странице (он же перход к якорю)
 */

function smoothlyScroll(){
    var linkNav = document.querySelectorAll('.b-head-nav-item');
    var speed = .5;
    for( i=0 ; i < linkNav.length; i++){
        linkNav[i].addEventListener('click', function(){
            var scrollTop = window.pageYOffset;
            var hash = this.href.replace(/[^#]*(.*)/, '$1');
            var elemToScroll = document.querySelector(hash).getBoundingClientRect().top -50;
            var start = null;

            requestAnimationFrame(step);
            function step(time){
                if (start === null){
                    start = time;
                }
                var progress = time - start;
                var distance = (elemToScroll < 0
                    ? Math.max(scrollTop - progress/speed, scrollTop + elemToScroll)
                    : Math.min(scrollTop + progress/speed, scrollTop + elemToScroll) );
                //distance -= 140;
                window.scrollTo(0,distance);
                if(distance != scrollTop + elemToScroll){
                    requestAnimationFrame(step);
                } else {
                    location.hash = hash;
                }
            }
            return false;
        } ,false);


    }
}
window.addEventListener('load', function(){
    smoothlyScroll();
} ,false);

/*
Спойлеры на экране "Варианты оплаты"
*/
function doPaySpoilText(obj, type){
    var parentNode = obj.parentNode.parentNode;
    var hideNode = parentNode.querySelector('.b-pay-one-spoil__text-spoil');
    var hideNodeHeight = 0;
    howHeightHideNode();
    console.log(hideNodeHeight);
    if (type == 'open'){
        parentNode.querySelector('.b-pay-one-spoil__text-spoil').style.maxHeight = hideNodeHeight +'px';
    } else {
        parentNode.querySelector('.b-pay-one-spoil__text-spoil').style.maxHeight = '0';
    }

    //Высчитываем высоту скрытого блока
    function howHeightHideNode(){
        var arrowHideNode = hideNode.querySelectorAll('p');
        for(i=0; i < arrowHideNode.length; i++){
            pHeight = arrowHideNode[i].clientHeight || arrowHideNode[i].offsetHeight+5;
            hideNodeHeight += pHeight;
        }
        return hideNodeHeight;
    }

}

function clickPaySpoilControl(){
    var controlBut = document.querySelectorAll('.b-pay-one-spoil__text-control');
    for(i=0; i < controlBut.length; i++){
        controlBut[i].addEventListener('click', function(e){
            var clickNode = e.target;
            if (clickNode.classList.contains('show-spoil')){
                clickNode.parentNode.querySelector('.close-spoil').style.display = 'block';
                clickNode.style.display = 'none';
                doPaySpoilText(e.target, 'open');
            } else {
                clickNode.parentNode.querySelector('.show-spoil').style.display = 'block';
                clickNode.style.display = 'none';
                doPaySpoilText(e.target, 'close');
            }
        } , false)
    }
}
window.addEventListener('load', function(){
    clickPaySpoilControl();
} ,false);


/*
 Табы на экране "Варианты оплаты"
 */

function clickPayTabsControl(){
    var controlButton = document.querySelectorAll('.b-p-b-tabs-head__item');

    for(i=0; i < controlButton.length; i++){
        controlButton[i].addEventListener('click', function(e){
            var clickNode = e.target;
            var allTextBlocks = document.querySelectorAll('.b-pay-tabs-info__item');
            var clickNodeParentChilde = clickNode.parentNode.querySelectorAll('.b-p-b-tabs-head__item');
            for(J=0; J < clickNodeParentChilde.length; J++){
                if(clickNode == clickNodeParentChilde[J]){
                    console.log(J);
                    allTextBlocks[J].style.display = 'block';
                } else {
                    allTextBlocks[J].style.display = 'none';
                }
            }
        } ,false)
    }
}

window.addEventListener('load', function(){
    clickPayTabsControl();
}, false);


/*
 Табы на экране "Варианты оплаты (Ипотека)"
 */
function creditTabsSetId(){
    var creditControl = document.querySelectorAll('.b-pay-credit-control-button__item'),
        creditText = document.querySelectorAll('.b-pay-cred-box-content__item'),
        creditInfoNum = document.querySelector('.credit-info-box-data').getAttribute('data-credittabs');
    for(i=0; i < creditControl.length; i++){
        if (creditControl[i].classList.contains('b-pay-credit-control-button__item--not-active') != true){
            creditControl[i].classList.add('b-pay-credit-control-button__item--not-active');
        }
        creditControl[creditInfoNum].classList.remove('b-pay-credit-control-button__item--not-active');

    }

    for(i=0; i < creditText.length; i++){
        creditText[i].style.display = 'none';
    }
    creditText[creditInfoNum].style.display = 'block';

}
function creditTabsGetId(){
    var creditControl = document.querySelectorAll('.b-pay-credit-control-button__item'),
        creditInfoNum = document.querySelector('.credit-info-box-data');
    for (i=0; i < creditControl.length; i++){
        creditControl[i].addEventListener('click', function(e){
            var nodeClick = e.target;
            console.log(nodeClick);
            var nodeClickParentChilde = nodeClick.parentNode.parentNode.querySelectorAll('.b-pay-credit-control-button__item p');
            console.log(nodeClickParentChilde);
           for (j=0; j < nodeClickParentChilde.length; j++){
                console.log(nodeClickParentChilde[j]);
                if (nodeClickParentChilde[j] == nodeClick){
                    console.log(j);
                    creditInfoNum.setAttribute('data-credittabs', j);
                    creditTabsSetId();
                } else {
                    console.log('что-то пошло не так');
                }
            }
        }, false)
    }
}

window.addEventListener('load', function(){
    creditTabsGetId();
    creditTabsSetId();
} , false);

/*
 Табы на экране "Новости"
*/

function clickNewsTabsControl(){
    var newsControlButton = document.querySelectorAll('.b-news-control-item');
    var newsControlString = document.querySelectorAll('.b-news-head__string');
    var dataIdTabs = document.querySelector('.b-news-body-super-secret-box').getAttribute('data-id');
    for(i=0; i < newsControlButton.length; i++){
        newsControlButton[i].addEventListener('click', function(e){
            var nodeClick = e.target;
            var controlItem = nodeClick.parentNode.parentNode;
            var allControlItem = document.querySelectorAll('.b-news-control-item');
            for (J=0; J < allControlItem.length; J++){
                if(allControlItem[J] == controlItem){
                    dataIdTabs = J;
                    setAttrib(dataIdTabs);
                }
            }

        } , false);
    }
    for(i=0; i < newsControlString.length; i++){
        newsControlString[i].addEventListener('click', function(e){
            var nodeClick = e.target;
            if(nodeClick.classList.contains('b-news-head__string--right')){
                dataIdTabs++;
                if (dataIdTabs >= 0 && dataIdTabs <=2){ //Првоеряем, чтобы значение dataIdTAbs не выходило за количество вкладок
                    setAttrib(dataIdTabs);
                } else if (dataIdTabs < 0) {
                    dataIdTabs = 0;
                    setAttrib(dataIdTabs);
                } else {
                    dataIdTabs = 2;
                    setAttrib(dataIdTabs);
                }
            } else {
                dataIdTabs--;
                if (dataIdTabs >= 0 && dataIdTabs <=2){
                    setAttrib(dataIdTabs);
                } else if (dataIdTabs < 0) {
                    dataIdTabs = 0;
                    setAttrib(dataIdTabs);
                } else {
                    dataIdTabs = 2;
                    setAttrib(dataIdTabs);
                }
            }

        } , false)
    }

    function setAttrib(num){
        infoBox = document.querySelector('.b-news-body-super-secret-box');
        var infoBoxNow = infoBox.getAttribute('data-id');
        if (infoBoxNow != num){
            infoBox.setAttribute('data-id', num);
            changeSlide();

        } else {
            return;
        }
    }
    function changeSlide(){
        var num = document.querySelector('.b-news-body-super-secret-box').getAttribute('data-id');
        var textBox = document.querySelectorAll('.b-news-text__item');
        var controlBut = document.querySelectorAll('.b-news-control-item');
        for(i=0; i < textBox.length; i++){
            if(i == num){
                textBox[i].classList.add('b-news-text__item--show');
            } else {
                if(textBox[i].classList.toggle('b-news-text__item--show')){
                    textBox[i].classList.remove('b-news-text__item--show');
                }
            }
        }
        for(i=0; i < controlBut.length; i++){
            if (controlBut[i].classList.toggle('b-news-control-item--checked')){
                controlBut[i].classList.remove('b-news-control-item--checked');
            }
            controlBut[num].classList.add('b-news-control-item--checked');

        }


    }
}

window.addEventListener('load', function(){
    clickNewsTabsControl();
} , false);


/*
 Табы на экране "О компании" (2-й скролл)
 */
function aboutAtributeToSlide(){
    var num = document.querySelector('.script-settings-about').getAttribute('data-control'),
        changeBlocks = document.querySelectorAll('.script-block-about'),
        changeControl = document.querySelectorAll('.script-control-about');
    for(i=0; i < changeBlocks.length; i++){
        if(i == num){
            changeBlocks[i].style.display = 'block';
        } else {
            changeBlocks[i].style.display = 'none';
        }
    }
    for(i=0; i < changeControl.length; i++){
        if(changeControl[i].classList.toggle('b-book-navigation__item--check')){
            changeControl[i].classList.remove('b-book-navigation__item--check');
        }

    }
    changeControl[num].classList.add('b-book-navigation__item--check');
}

function aboutSetDataForTabs(){
    var controlButton = document.querySelectorAll('.script-control-about');
    for(i=0; i < controlButton.length; i++){
        controlButton[i].addEventListener('click', function(e){
            var clickNode = e.target,
                clickNodeParentChilde = clickNode.parentNode.querySelectorAll('.script-control-about'),
                controlNode = document.querySelector('.script-settings-about');
            for (J=0; J < clickNodeParentChilde.length; J++){
                if(clickNodeParentChilde[J] == clickNode){
                    controlNode.setAttribute('data-control', J);
                    aboutAtributeToSlide();
                }
            }


        }, false)
    }
}

window.addEventListener('load', function(){
    aboutSetDataForTabs();
    aboutAtributeToSlide();
} , false);





/*
 Табы на экране "Квартиры" (4-й скролл)
 */
function planAtributeToSlide(){
    var num = document.querySelector('.script-settings-plan').getAttribute('data-control'),
        changeBlocks = document.querySelectorAll('.script-block-plan'),
        changeControl = document.querySelectorAll('.script-control-plan');
    for(i=0; i < changeBlocks.length; i++){
        if(i == num){
            changeBlocks[i].style.display = 'block';
        } else {
            changeBlocks[i].style.display = 'none';
        }
    }
    for(i=0; i < changeControl.length; i++){
        if(changeControl[i].classList.toggle('b-book-navigation__item--check')){
            changeControl[i].classList.remove('b-book-navigation__item--check');
        }

    }
    changeControl[num].classList.add('b-book-navigation__item--check');

}

function planSetDataForTabs(){
    var controlButton = document.querySelectorAll('.script-control-plan');
    for(i=0; i < controlButton.length; i++){
        controlButton[i].addEventListener('click', function(e){
            var clickNode = e.target,
                clickNodeParentChilde = clickNode.parentNode.querySelectorAll('.script-control-plan'),
                controlNode = document.querySelector('.script-settings-plan');

            for (J=0; J < clickNodeParentChilde.length; J++){
                if(clickNodeParentChilde[J] == clickNode){
                    controlNode.setAttribute('data-control', J);
                    planAtributeToSlide();
                }
            }

        }, false)
    }
}

window.addEventListener('load', function(){
    planSetDataForTabs();
    planAtributeToSlide();
} , false);



/*
 Табы на экране "Контакты" (Последний скролл)
 */
function contactAtributeToSlide(){
    var num = document.querySelector('.script-settings-contact').getAttribute('data-control'),
        changeBlocks = document.querySelectorAll('.script-block-contact'),
        changeControl = document.querySelectorAll('.script-control-contact');
    for(i=0; i < changeBlocks.length; i++){
        if(i == num){
            changeBlocks[i].style.display = 'block';
        } else {
            changeBlocks[i].style.display = 'none';
        }
    }
    for(i=0; i < changeControl.length; i++){
        if(changeControl[i].classList.toggle('b-book-navigation__item--check')){
            changeControl[i].classList.remove('b-book-navigation__item--check');
        }

    }
    changeControl[num].classList.add('b-book-navigation__item--check');

}

function contactSetDataForTabs(){
    var controlButton = document.querySelectorAll('.script-control-contact');
    for(i=0; i < controlButton.length; i++){
        controlButton[i].addEventListener('click', function(e){
            var clickNode = e.target,
                clickNodeParentChilde = clickNode.parentNode.querySelectorAll('.script-control-contact'),
                controlNode = document.querySelector('.script-settings-contact');

            for (J=0; J < clickNodeParentChilde.length; J++){
                if(clickNodeParentChilde[J] == clickNode){
                    controlNode.setAttribute('data-control', J);
                    contactAtributeToSlide();
                }
            }

        }, false)
    }
}

window.addEventListener('load', function(){
    contactSetDataForTabs();
    contactAtributeToSlide();
} , false);

/*
   Смена менеджеров на экране "Контакты"
*/

function saleManagerSlider(){
    var slide = document.querySelector('.b-sales-manager-slide'),
        slideControlLeft = slide.querySelector('.b-sales-manager-slide__left-arrow'),
        slideControlRight = slide.querySelector('.b-sales-manager-slide-right-arrow'),
        oneManager = slide.querySelectorAll('.b-sales-manager-slide-one-manager'),
        managerNameInForm = document.querySelectorAll('.b-sales-form-input-disabled'),
        num = slide.getAttribute('data-id');
    num *= 1;

    function changeManagerOnAttribut(){

        for(i=0; i < oneManager.length; i++){
            oneManager[i].style.display = 'none';
        }
        oneManager[num].style.display = 'block';

        for(i=0; i < managerNameInForm.length; i++){
            managerNameInForm[i].style.display = 'none';
        }
        managerNameInForm[num].style.display = 'block';

    }
    changeManagerOnAttribut();

    slideControlLeft.addEventListener('click', function(){
        num -= 1;
        if (num < 0){
            num = oneManager.length - 1;
        }
        slide.setAttribute('data-id', num);
        changeManagerOnAttribut();
    } ,false);


    slideControlRight.addEventListener('click', function(){
        num += 1;
        if(num > oneManager.length - 1){
            num = 0;
        }
        slide.setAttribute('data-id', num);
        changeManagerOnAttribut();
    } , false)
}

window.addEventListener('load', function(){
    saleManagerSlider();
} , false);


/*
 Смена менеджеров на экране "Контакты" (Отдел Ипотеки)
 */

function saleManagerSliderCredit(){
    var slide = document.querySelector('.b-sales-manager-slide--credit'),
        slideControlLeft = slide.querySelector('.b-sales-manager-slide__left-arrow'),
        slideControlRight = slide.querySelector('.b-sales-manager-slide-right-arrow'),
        oneManager = slide.querySelectorAll('.b-sales-manager-slide-one-manager'),
        managerNameInForm = document.querySelectorAll('.b-sales-form-input-disabled--credit'),
        num = slide.getAttribute('data-id');
    num *= 1;

    function changeManagerOnAttribut(){

        for(i=0; i < oneManager.length; i++){
            oneManager[i].style.display = 'none';
        }
        oneManager[num].style.display = 'block';

        for(i=0; i < managerNameInForm.length; i++){
            managerNameInForm[i].style.display = 'none';
        }
        managerNameInForm[num].style.display = 'block';

    }
    changeManagerOnAttribut();

    slideControlLeft.addEventListener('click', function(){
        num -= 1;
        if (num < 0){
            num = oneManager.length - 1;
        }
        slide.setAttribute('data-id', num);
        changeManagerOnAttribut();
    } ,false);


    slideControlRight.addEventListener('click', function(){
        num += 1;
        if(num > oneManager.length - 1){
            num = 0;
        }
        slide.setAttribute('data-id', num);
        changeManagerOnAttribut();
    } , false)



}

window.addEventListener('load', function(){
    saleManagerSliderCredit();
} , false);



/*
Горизонтальный спойлер
 */

function horizontalSpoil(){
    var allSpoilName = document.querySelectorAll('.b-hor-spoil-control__name'),
        allSpoilBut = document.querySelectorAll('.b-hor-spoil-control__but'),
        oneSpoilBox;

    //Собираем клики и находим родительский блок
    for(i=0; i < allSpoilName.length; i++){
        allSpoilName[i].addEventListener('click', function(e){
            var clickNode = e.target;

            oneSpoilBox = clickNode.parentNode.parentNode.parentNode;
            console.log(oneSpoilBox);
            changeDataOpen();
            return oneSpoilBox;


        } , false);
    }
    for(i=0; i < allSpoilBut.length; i++){
        allSpoilBut[i].addEventListener('click', function(e){
            var clickNode = e.target;
            oneSpoilBox = clickNode.parentNode.parentNode.parentNode;
            changeDataOpen();
            return oneSpoilBox;

        } , false);
    }

    function changeDataOpen(){
        var box = oneSpoilBox,
            num = box.getAttribute('data-open');

        if(num == 0){
            box.setAttribute('data-open', '1');
            changeConditionSpoil();

        } else {
            box.setAttribute('data-open', '0');
            changeConditionSpoil();

        }


    }
    //В родительском блоке производим манипуляции по открытию и закрытию
    function changeConditionSpoil(){
        var box = oneSpoilBox,
            num = box.getAttribute('data-open'),
            spoilText = box.querySelector('.b-hor-spoil-text'),
            spoilTextHeight = spoilText.querySelector('.b-hor-spoil-text__p').clientHeight + 20 || spoilText.offsetHeight + 20;

        if (num == 0){
            box.classList.remove('b-horizontal-spoil-open');
            spoilText.style.maxHeight = 0;
        } else {
            box.classList.add('b-horizontal-spoil-open');
            spoilText.style.maxHeight = spoilTextHeight+'px';

        }

    }

}

window.addEventListener('load', function(){
    horizontalSpoil();
}, false);


/*
  Скрипт ротатора (скрипт один для всех ротаторов)
*/

function rotator(){

    //Ловим события, чтобы для того, чтобы определить в каком именно ротаторе оно произошло

    var allRotatorLinkLeft = document.querySelectorAll('.b-rotator-body__control--left-str'),
        allRotatorLinkRight = document.querySelectorAll('.b-rotator-body__control--right-str'),
        allRotatorControl = document.querySelectorAll('.b-rotator-head-link'),
        rotatorBlock;



    for(i=0; i < allRotatorLinkLeft.length ; i++){
        allRotatorLinkLeft[i].addEventListener('click', function(e){
          rotatorBlock = e.target.parentNode.parentNode.parentNode;
            changeRotatorDataPrev();
        } ,false)

    }
    for(i=0; i < allRotatorLinkRight.length ; i++){
        allRotatorLinkRight[i].addEventListener('click', function(e){
            rotatorBlock = e.target.parentNode.parentNode.parentNode;
            changeRotatorDataNext();
        } ,false)

    }

    for(i=0; i < allRotatorControl.length ; i++){
        allRotatorControl[i].addEventListener('click', function(e){
            rotatorBlock = e.target.parentNode.parentNode.parentNode.parentNode;

            //Ищем кнопки управления, чтобы найти номер кнопки, по которой произощёл вызов

            var controlInRotator = rotatorBlock.querySelectorAll('.b-rotator-head-link');

            for(z=0; z < controlInRotator.length; z++){
                if(controlInRotator[z] == e.target){
                    changeRotatorDataControl(z);
                }
            }

        } , false)

    }

    //Теперь работаем с конкретным ротатором

    function changeRotatorDataPrev(){
        var data = rotatorBlock.getAttribute('data-slide')*1;
        data -= 1;
        if(data < 0){
            data = 0;
        }
        rotatorBlock.setAttribute('data-slide', data);
        changeRotator();
    }
    function changeRotatorDataNext(){
        var data = rotatorBlock.getAttribute('data-slide')* 1,
            slide = rotatorBlock.querySelectorAll('.b-rotator-body-col-content-item');
        data += 1;
        if (data > slide.length - 1){
            data = slide.length - 1;
        }
        rotatorBlock.setAttribute('data-slide', data);
        changeRotator();
    }

    function changeRotatorDataControl(num){
        var data = rotatorBlock.getAttribute('data-slide')* 1;
        data = num;
        rotatorBlock.setAttribute('data-slide', data);
        changeRotator();
    }

    //В соотвествии с data показываем слайды

    function changeRotator(){
        var data = rotatorBlock.getAttribute('data-slide'),
            control = rotatorBlock.querySelectorAll('.b-rotator-head-link'),
            slide = rotatorBlock.querySelectorAll('.b-rotator-body-col-content-item');

        for(i=0; i < control.length; i++){
            if(control[i].classList.toggle('b-rotator-head-link--checked')){
                control[i].classList.remove('b-rotator-head-link--checked');
            }
        }
        control[data].classList.add('b-rotator-head-link--checked');

        for(i=0; i < slide.length; i++){
            if(slide[i].classList.toggle('b-rotator-body-col-content-item--show')){
                slide[i].classList.remove('b-rotator-body-col-content-item--show');
            }
        }
        slide[data].classList.add('b-rotator-body-col-content-item--show');

    }




}

window.addEventListener('load', function(){
    rotator();
} ,false);



/*
Модальное окно планировка
 */

function modalPlan(){
    //Создаем событие, открывающее модельное окно
    var showModalElement = document.querySelectorAll('.show-modal'),
        showModalElementSRC = [],
        modal = document.querySelector('.b-modalWindow'),
        leftBut = document.querySelector('.b-modalContent-body-contol-left'),
        rightBut = document.querySelector('.b-modalContent-body-contol-right'),
        closeBut = document.querySelector('.b-modalContent-head-close'),
        closeBg = document.querySelector('.b-modalBg'),
        printBut = document.querySelector('.b-modalContent-head-print');

    for(i=0; i < showModalElement.length; i++){
        showModalElement[i].addEventListener('click', function(e){
            var nodeClick = e.target,
                nodeParentChilde = nodeClick.parentNode.parentNode.parentNode,
                parentArr = nodeParentChilde.querySelectorAll('.show-modal');
            console.log(e.target);
            console.log(nodeParentChilde);
            for(z=0 ; z < parentArr.length; z++){
                if(nodeClick == parentArr[z]){
                    modal.setAttribute('data-img', z); //Устанавливаем номер изображения для открытия
                    setImg();
                }
            }
            modal.setAttribute('data-open', '1'); //Устанавливаем атрибут на открытие
            openModal();
        }, false);
        showModalElementSRC[i] = showModalElement[i].getAttribute('src'); //Создаем массив с ссылками на изображения
    }

    //Собираем клики по переключению картинок в открытом модельном окне

    leftBut.addEventListener('click', function(){
        var numDataImg = modal.getAttribute('data-img')*1;
        if(numDataImg > 0){
            numDataImg -= 1;
            console.log(numDataImg);
            modal.setAttribute('data-img', numDataImg);
            setImg()
        }
    } ,false);

    rightBut.addEventListener('click', function(){
        var numDataImg = modal.getAttribute('data-img')*1;
        if(numDataImg < showModalElement.length - 1){
            numDataImg += 1;
            console.log(numDataImg);
            modal.setAttribute('data-img', numDataImg);
            setImg()
        }
    } ,false);


    //Ловим события закрывающие модальное окно

    closeBut.addEventListener('click', function(){
        modal.setAttribute('data-open', '0');
        openModal()
    } ,false);

    closeBg.addEventListener('click', function(){
        modal.setAttribute('data-open', '0');
        openModal()
    } ,false);

    //Делаем кнопку принт

    printBut.addEventListener('click', function(){
        print();
    }, false);

    function setImg(){
        var img = modal.querySelector('.b-modalContent-body-img'),
            num = modal.getAttribute('data-img');
        console.log(img.innerHTML = '<img class="b-big-modal-img" src="'+ showModalElementSRC[num] +'" alt=""/>');
        img.innerHTML = '<img class="b-big-modal-img" src="'+ showModalElementSRC[num] +'" alt=""/>'




    }
    function openModal(){
        if (modal.getAttribute('data-open') == 0){
            modal.style.display = 'none';
        } else {
            modal.style.display = 'block';
        }

    }
    //Создаем всплывающее окно
}

window.addEventListener('load', function(){
    modalPlan();
} ,false);



/*
 Модальное окно отделка
 */
function modalRem(){
    //Создаем событие, открывающее модельное окно
    var showModalElement = document.querySelectorAll('.show-modal-rem'),
        showModalElementSRCrem = [],
        modal = document.querySelector('.b-modalWindow-rem'),
        leftBut = document.querySelector('.b-modalContent-body-contol-left-rem'),
        rightBut = document.querySelector('.b-modalContent-body-contol-right-rem'),
        closeBut = document.querySelector('.b-modalContent-head-close-rem'),
        closeBg = document.querySelector('.b-modalBg-rem');

    for(i=0; i < showModalElement.length; i++){
        showModalElement[i].addEventListener('click', function(e){
            var nodeClick = e.target,
                nodeParentChilde = nodeClick.parentNode.parentNode.parentNode,
                parentArr = nodeParentChilde.querySelectorAll('.show-modal-rem');
            for(z=0 ; z < parentArr.length; z++){
                if(nodeClick == parentArr[z]){
                    modal.setAttribute('data-img', z); //Устанавливаем номер изображения для открытия
                    setImg();
                }
            }
            modal.setAttribute('data-open', '1'); //Устанавливаем атрибут на открытие
            openModal();
        }, false);
        showModalElementSRCrem[i] = showModalElement[i].getAttribute('src'); //Создаем массив с ссылками на изображения
    }

    //Собираем клики по переключению картинок в открытом модельном окне

    leftBut.addEventListener('click', function(){
        var numDataImg = modal.getAttribute('data-img')*1;
        if(numDataImg > 0){
            numDataImg -= 1;
            modal.setAttribute('data-img', numDataImg);
            setImg()
        }
    } ,false);

    rightBut.addEventListener('click', function(){
        var numDataImg = modal.getAttribute('data-img')*1;
        if(numDataImg < showModalElement.length - 1){
            numDataImg += 1;
            modal.setAttribute('data-img', numDataImg);
            setImg()
        }
    } ,false);


    //Ловим события закрывающие модальное окно

    closeBut.addEventListener('click', function(){
        modal.setAttribute('data-open', '0');
        openModal()
    } ,false);

    closeBg.addEventListener('click', function(){
        modal.setAttribute('data-open', '0');
        openModal()
    } ,false);

    //Создаем всплывающее окно
    function openModal(){
        if (modal.getAttribute('data-open') == 0){
            modal.style.display = 'none';
        } else {
            modal.style.display = 'block';
            setImg();
        }

    }
    function setImg(){
        var img = modal.querySelector('.b-modalContent-body-img-rem'),
            num = modal.getAttribute('data-img');
        console.log(img);
        img.innerHTML = '<img class="b-big-modal-img" src="'+ showModalElementSRCrem[num] +'" alt=""/>'



    }


}


window.addEventListener('load', function(){
    modalRem();
} ,false);


/*
Проверка форм на заполненость
*/

function controlForm(){
    var forms = document.querySelectorAll('.control-form');

    for(i=0; i < forms.length; i++){
        forms[i].addEventListener('blur', function(e){
            var clickNode = e.target;
            if(clickNode.value ==  ''){
                clickNode.style.borderColor = 'red';
            } else {
                clickNode.style.borderColor = '#000';
            }
        }, false)
    }
}

window.addEventListener('load', function(){
    controlForm();
}, false);

window.addEventListener('load', function(){
    compliteSendMail();
} ,false);


/*Форма подписаться на новости*/

function showSubscribe(){
    var box = document.querySelector('.b-subscribe'),
        closeBg = box.querySelector('.b-subscribe-bg'),
        butOpen = document.querySelector('.b-new-subscribe__link--rss');
    butOpen.addEventListener('click', function(){
        box.setAttribute('data-open', '1');
        chuseOpen();
    }, false);

    closeBg.addEventListener('click', function(){
        box.setAttribute('data-open', '0');
        chuseOpen();
    }, false)

    function chuseOpen(){
        var num = box.getAttribute('data-open');
        if(num == 0){
            box.style.display = 'none';
            console.log(num);

        } else {
            box.style.display = 'block';
        }
    }

}

window.addEventListener('load', function(){
    showSubscribe();
},false);

/*Модальное окно генплана*/

function genPlanModal(){
    var plan = document.querySelector('.b-book-text-2-column__img'),
        planSrc = plan.getAttribute('src'),
        modal = document.querySelector('.b-modalWindow'),
        modalInner = document. querySelector('.b-modalContent-body-img'),
        leftStr = document.querySelector('.b-modalContent-body-contol-left'),
        rightStr = document.querySelector('.b-modalContent-body-contol-right');

    plan.addEventListener('click', function(){
        modal.setAttribute('data-open', '1');
        openModal();
    },false);

    function openModal(){
        if (modal.getAttribute('data-open') == 0){
            modal.style.display = 'none';
            modalInner.style.width = '90%';
            leftStr.style.display = 'block';
            rightStr.style.display = 'block';
        } else {
            modal.style.display = 'block';
            modalInner.style.width = '100%';
            leftStr.style.display = 'none';
            rightStr.style.display = 'none';
            setImg();
        }
    }
    function setImg(){
        console.log(planSrc);
        modalInner.innerHTML = '<img class="b-big-modal-img" style="  width: auto;  max-height: 700px; margin: 0 auto; display: block;" src="'+ planSrc +'" alt=""/>'
    }
}

window.addEventListener('load', function(){
    genPlanModal();
},false);